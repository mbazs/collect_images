from haversine import haversine
import glob
import os
import shutil
import piexif
from pathlib import Path

scan_dir='./iphone-mbazs/**/*.[jJ][pP][gG]'
target_dir = "Helyszinek"
places={
    "Kecskemet" :            { "gps": (46.907400, 19.691700), "radius": [ 10, 50 ] },
    "Izsak" :                { "gps": (46.800470, 19.359610), "radius": [ 5, 20 ] },
    "Izsak_Kodaly_utca" :    { "gps": (46.805834, 19.359110), "radius": [ 0.2] },
    "Csorna_lakas" :         { "gps": (47.612078, 17.247559), "radius": [ 0.1, 2, 30 ] },
    "Farad_uj_haz" :         { "gps": (47.603587, 17.206876), "radius": [ 0.2 ] },
    "Salfold" :              { "gps": (46.836540, 17.549090), "radius": [ 5, 20 ] },
    "Keszthely" :            { "gps": (46.769650, 17.248130), "radius": [ 2, 30 ] },
    "Izsak_Rakoczi_utca" :   { "gps": (46.798730, 19.361097), "radius": [ 0.2 ] },
    "Dunakeszi_strand" :     { "gps": (47.650789, 19.119842), "radius": [ 0.5 ] },
    "Eger" :                 { "gps": (47.898889, 20.374722), "radius": [ 10, 50 ] },
    "Bp_Sasadi_ut" :         { "gps": (47.475457, 19.004366), "radius": [ 0.2 ] }
}

def main():
    shutil.rmtree(target_dir, True)
    os.mkdir(target_dir)
    files = glob.glob(scan_dir, recursive=True)
    for file in files:
        realfile = Path(file).resolve()
        filename = os.path.basename(file)
        exif_dict = piexif.load(file)
        lat, latdir, long, longdir, date = (None, None, None, None, None)
        for tag in exif_dict["GPS"]:
            name = piexif.TAGS["GPS"][tag]["name"]
            value = exif_dict["GPS"][tag]
            if name == "GPSLatitude":
                lat = value
            if name == "GPSLatitudeRef":
                latdir = value
            if name == "GPSLongitude":
                long = value
            if name == "GPSLongitudeRef":
                longdir = value
        for tag in exif_dict["Exif"]:
            name = piexif.TAGS["Exif"][tag]["name"]
            value = exif_dict["Exif"][tag]
            if name == "DateTimeOriginal":
                v2 = value.decode("ascii")
                date = v2[:v2.find(" ")].replace(":", "-")
        if lat is not None and latdir is not None and long is not None and longdir is not None:
            dd = (dms2dd(lat[0], lat[1], lat[2], latdir), dms2dd(long[0], long[1], long[2], longdir))
            for place in places.keys():
                imgdir = target_dir + "/" + place
                ensure_dir(imgdir)
                for radius in places[place]["radius"]:
                    if haversine(dd, places[place]["gps"]) <= radius:
                        radiuslabel = place + "_" + (str(radius) + "_km-es" if radius >= 1 else str(round(radius * 1000)) + "_meteres") + "_korzete"
                        radiusdir = imgdir + "/" + radiuslabel
                        ensure_dir(radiusdir)
                        os.symlink(realfile, radiusdir + "/" + filename)
                        if date is not None:
                            datedir = radiusdir + "/Datumok/" + date
                            ensure_dir(datedir)
                            os.symlink(realfile, datedir + "/" + filename)


def dms2dd(deg, minutes, seconds, direction):
    return (float(deg[0]/deg[1]) + float(minutes[0]/minutes[1]) / 60 + float(seconds[0]/seconds[1]) / (60 * 60)) * (-1 if direction in ['W', 'S'] else 1)


def ensure_dir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)


if __name__ == "__main__":
    main()

